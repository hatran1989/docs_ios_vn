.. GTVSDKDoc documentation master file, created by
   sphinx-quickstart on Thu Mar 14 15:46:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Setup SDK & Environment
=====================================

Technical Requirements
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
iOS 10 as a deployment target.

Download SDK 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
`Download now <http://google.com.vn>`_

GTVCocoaTouchFramework 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Drag and drop GTVCocoaTouchFramework.framework to your project.

.. image:: /images/img1.png

Click Finish

.. image:: /images/img2.png

Add GTVCocoaTouchFramework to Embedded Binary:

.. image:: /images/img3.png

Import GTVCocoaTouchFramework to App Delegate::

	#import <GTVCocoaTouchFramework/GTVCocoaTouchFramework.h>

In AppDelegate.m::

	- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	  [GTVManager handleDidFinishLaunchingWithOptions:launchOptions];
	  if ([GTVManager getUserHash]) {
	  	// If logged in -> Goto game scene
	  } else {
	  	// do nothing...login/register
	  }
	  return YES;
	}

	- (void)applicationWillResignActive:(UIApplication *)application {
	  [GTVManager handleWillResignActive];
	}

	- (void)applicationDidEnterBackground:(UIApplication *)application {
	  [GTVManager handleDidEnterBackground];
	}

	- (void)applicationWillEnterForeground:(UIApplication *)application {
	  [GTVManager handleWillEnterForeground];
	}

	- (void)applicationDidBecomeActive:(UIApplication *)application {
	  [GTVManager handleDidBecomeActive];
	}

	- (void)applicationWillTerminate:(UIApplication *)application {
	  [GTVManager handleWillTerminate];
	}

Build Settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Add value for “Other Linker Flags”: -all_load -ObjC -lc++

Change value for “Allow Non-modular Includes in Framework Modules”: Yes


Info.plist config
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Now configure the .plist for your project: In Xcode right-click your .plist file and choose “Open As Source Code”.Copy & Paste the XML snippet into the body of your file (…).
– Config for login via facebook::
	
	<key>FacebookAppID</key>
	<string>{FACEBOOK_APP_ID}</string>
	<key>FacebookDisplayName</key>
	<string>{FACEBOOK_APP_NAME}</string>
	<key>CFBundleURLTypes</key>
	<array>
	    <dict>
	        <key>CFBundleTypeRole</key>
	        <string>Editor</string>
	        <key>CFBundleURLName</key>
	        <string></string>
	        <key>CFBundleURLSchemes</key>
	        <array>
	            <string>fb{Facebook_App_Id}</string>
	        </array>
	    </dict>
	    <dict>
	        <key>CFBundleTypeRole</key>
	        <string>Editor</string>
	        <key>CFBundleURLName</key>
	        <string></string>
	        <key>CFBundleURLSchemes</key>
	        <array>
	            <string>{Bundle_Identifier}</string>
	        </array>
	    </dict>
	</array>

**Note: {Facebook_App_Id} and {Bundle_Identifier}, we will send private for you **

- Config for URL Types::

	<key>CFBundleURLTypes</key>
		<array>
			<dict>
				<key>CFBundleTypeRole</key>
				<string>Editor</string>
				<key>CFBundleURLSchemes</key>
				<array>
					<string>{GOOGLE_REVERSED_CLIENT_ID}</string>
				</array>
			</dict>
		</array>

**Note: {REVERSED_CLIENT_ID}, we will send private for you **

- Config App Transport Security::

	<key>NSAppTransportSecurity</key>
		<dict>
			<key>NSAllowsArbitraryLoads</key>
			<true/>
		</dict>

