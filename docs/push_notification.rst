.. GTVSDKDoc documentation master file, created by
   sphinx-quickstart on Thu Mar 14 15:46:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Remote push notification
=====================================

Enable Push Notification
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Goto Capabilities -> Push Notification -> Turn On

.. image:: /images/img4.png

Enable Background Mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Goto Capabilities -> Background Modes -> Turn On
Check to Background fetch, Remote notifications

.. image:: /images/img5.png

GoogleService-Info.plist
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Note: GoogleService-Info.plist, we will send private for you **

Add some code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Import GTVCocoaTouchFramework and add FirebaseServiceDelegate to AppDelegate.m :: 

	#import <GTVCocoaTouchFramework/GTVCocoaTouchFramework.h>

	@interface AppDelegate () <FirebaseServiceDelegate>

	@end

AppDelegate.m::

	- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	  [[FirebaseService sharedManager] configure];
	  [[FirebaseService sharedManager] setDelegate:self];
	  return YES;
	}

	#pragma mark - FirebaseServiceDelegate
	- (void)messagingDidReceiveMessage:(NSDictionary *)remoteMessage {
	    
	}

	- (void)messagingDidRefreshRegistrationToken:(nonnull NSString *)fcmToken {
	    [GTVManager postFcmToken:fcmToken];
	}