.. GTVSDKDoc documentation master file, created by
   sphinx-quickstart on Thu Mar 14 15:46:07 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Payment
=====================================

Import GTVCocoaTouchFramework to your class::

	#import <GTVCocoaTouchFramework/GTVCocoaTouchFramework.h>

- Add GTVManagerDelegate to your class::

	@interface YourClass ()<GTVManagerDelegate>

	@end

- Show payment::

	GTVManager.sharedManager.delegate = self;

    	[GTVManager.sharedManager showPayment];

- Handle GTVManagerDelegate::

	- (void)gtvDidPayWithUser:(NSString *)userID characterName:(NSString *)characterName packageName:(NSString *)packageName gameVersion:(NSString *)gameVersion {

	}